<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'WAiwzfO3HKLB6D9XZq1Qzme0eB2Noer2zQBY+sI9vbJQV4lN9LuQfiSIMCnBEzT+YUKfTx1mQodgPoqump5+sg==');
define('SECURE_AUTH_KEY',  'eczL478goFii1KAylPvdHoOYPg42AyfrUQLLLNQnvnv4CYf/WshUbedQyTHbdw4zhVMMBGCjbqkfmjTh0Td9uA==');
define('LOGGED_IN_KEY',    '0Qfo/hTlyRlZFD5R6gIvTZTmKk+lmLRkgTMAKc98mOaHtFoVFaImXEchMDH9/umSpQRr+KYZqeMb105/B9VaIw==');
define('NONCE_KEY',        'iXwDVijqFS8ZRPCv/F7VbeMsPKtSYdkJ66Q5A0bG2rHyv7sBdx5dYEIp1/3ed9uKr/sSZ722BL+KeDeRjI7JaQ==');
define('AUTH_SALT',        '8dkhuuXzW3zZl1pPfHYfRQRB/0O0QvC2k30DSeX7As+3j6jMlDJQLyos9O6s7h/MSAeVWGOQ5A0P22y2bmCDAg==');
define('SECURE_AUTH_SALT', 'JlZJvQLu3y1h4iWFux/vCYjBL6kO75Y5/yPCh5yNepwm1UmMGOmwe1nw/avXvlNJgAtY68IxYrpDmeec+JS/iA==');
define('LOGGED_IN_SALT',   'tdSdT02EzkqKcOP35J5pbF+Ceq/ELoNp7uroguKya5pmxthkBh/WMC70Oa3HdY7J+ZofGgtXX7T/cocdC8bhAA==');
define('NONCE_SALT',       'fJDLVfTrQ/K9fnUv/hlmUOtbZAB/13aZKnMhYdwJ8pjw9G32r+1bpj3WUudWNUSPivtRgIKW1idDxu+BoU3ruw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
